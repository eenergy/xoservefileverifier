﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace XoserveFileVerifier
{
    public sealed class XoserveDataProcessor
    {
        private static readonly Regex csvSplit = new Regex("(?:^|,)(\"(?:[^\"]+|\"\")*\"|[^,]*)", RegexOptions.Compiled);
        private readonly string[] files;

        public XoserveDataProcessor(IEnumerable<string> files)
            => this.files = files.ToArray();

        public IEnumerator<XoserveDataRecord>  GetEnumerator()
        {
            foreach (var file in files)
            {
                using (var stream = new StreamReader(file, Encoding.UTF8, true, 1024 * 1024 * 2))
                {
                    while (!stream.EndOfStream)
                    {
                        var line = stream.ReadLine();
                        if (ShouldSkip(line))
                        {
                            continue;
                        }
                        string[] rawFields = SplitLine(line);
                        var fields = FormatFields(rawFields);
                        var record = new XoserveDataRecord(fields);
                        if (ShouldSkip(record))
                        {
                            continue;
                        }
                        yield return record;
                    }
                }
            }
        }

        private static bool ShouldSkip(string line) => string.IsNullOrWhiteSpace(line);

        private static bool ShouldSkip(XoserveDataRecord record) => record.FieldCount < 20;

        private static string[] SplitLine(string line)
        {
            var matches = csvSplit.Matches(line);
            if (matches == null)
            {
                throw new ApplicationException($"Invalid CSV record: {line}");
            }
            var result = new string[matches.Count];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = "\"" + matches[i].Value.TrimStart(',').TrimStart('"').TrimEnd('"').Replace(",", "") + "\"";
            }
            if (result.Length > 18)
            {
                result[result.Length - 1] = result[16] + result[17];
            }
            return result;
        }

        private static object[] FormatFields(string[] fields)
        {
            if (fields.Length >= 10)
            {
                if (int.TryParse(fields[9].ToString().Trim('"'), out int result)) // Pad building_number with leading zeros
                {
                    fields[9] = $"\"{result.ToString().PadLeft(6, '0')}\"";
                }

                if (fields.Length >= 20) // Genreate post_code column from incode and outcode
                {
                    fields[19] = fields[16].ToString() + fields[17].ToString();
                }
            }
            return fields;
        }
    }
}
