﻿using System.Data;

namespace XoserveFileVerifier
{
    public sealed class XoserveDataRecord : ArrayDataRecord
    {
        internal static readonly string[] columns = new[] { "area", "file_type", "date", "temp1", "mprn", "msn", "principle_street", "sub_building", "dep_street", "building_number", "address_1", "address_2", "address_3", "address_4", "city", "County", "outcode", "incode", "temp2", "post_code" };
        private static readonly DataColumn[] dataColumns = GetColumns();

        private static DataColumn[] GetColumns()
        {
            var cols = new DataColumn[columns.Length];
            for (int i = 0; i < cols.Length; i++)
            {
                cols[i] = new DataColumn()
                {
                    ColumnName = columns[i],
                    DataType = typeof(string),
                    AllowDBNull = true
                };
            }
            return cols;
        }

        public XoserveDataRecord(params object[] fields) : base(dataColumns, fields)
        {
        }
    }
}
