﻿using System;
using System.Linq;

namespace XoserveFileVerifier
{
    class Program
    {
        // Command-line takes a list of files to scan
        static void Main(string[] args)
        {
            foreach (var filePath in args)
            {
                try
                {
                    ProcessFile(filePath);
                }
                catch (ApplicationException ex)
                {
                    Console.Error.WriteLine(ex.Message);
                }
            }
        }

        static void ProcessFile(string filePath)
        {
            ulong recordCounter = 0;
            var columnNames = XoserveDataRecord.columns;
            foreach (var record in new XoserveDataProcessor(new[] { filePath }))
            {
                ++recordCounter;
                for (int columnIndex = 0; columnIndex < columnNames.Length; ++columnIndex)
                {
                    var value = record.GetString(columnIndex);
                    switch (columnIndex)
                    {
                        default:
                            if (value.Length > 200)
                            {
                                throw new ApplicationException($"Column #{columnIndex} ({columnNames[columnIndex]}) of record {recordCounter} has is longer than 200 characters: {value.Length}");
                            }
                            break;
                        case 18: // temp2
                            if (value.Length > 50)
                            {
                                throw new ApplicationException($"Column #{columnIndex} ({columnNames[columnIndex]}) of record {recordCounter} has is longer than 50 characters: {value.Length}");
                            }
                            break;
                        case 19: // post_code
                            if (value.Length > 128)
                            {
                                throw new ApplicationException($"Column #{columnIndex} ({columnNames[columnIndex]}) of record {recordCounter} has is longer than 128 characters: {value.Length}");
                            }
                            break;
                    }
                }
            }
            Console.WriteLine($"File \"{filePath}\" ({recordCounter}): OK");
        }
    }
}
