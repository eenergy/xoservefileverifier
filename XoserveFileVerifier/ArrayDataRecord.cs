﻿using System;
using System.Data;
using System.Data.Common;

namespace XoserveFileVerifier
{
    public class ArrayDataRecord : DbDataRecord
    {
        private readonly DataColumn[] columns;
        private object[] fields;
        private readonly int fieldCount;
        public object[] Fields
        {
            get => fields;
            set
            {
                if (value.Length != fields.Length)
                {
                    throw new InvalidOperationException("Cannot change the field count after construction");
                }
                fields = value;
            }
        }

        public override int FieldCount => fieldCount;

        public System.Collections.Generic.IReadOnlyCollection<DataColumn> Columns => columns;

        public override object this[string name] => this[GetOrdinal(name)];

        public override object this[int i] => GetValue(i);

        public ArrayDataRecord(DataColumn[] columns, params object[] fields)
        {
            this.columns = columns ?? throw new ArgumentNullException(nameof(columns));
            if (fields == null)
            {
                throw new ArgumentNullException(nameof(fields));
            }

            this.fields = new object[columns.Length];
            fieldCount = fields.Length < columns.Length ? fields.Length : columns.Length;

            // Assign field values, changing types if neccecery
            for (int i = 0; i < fieldCount; i++)
            {
                this.fields[i] = fields[i] == null ? null : Convert.ChangeType(fields[i], columns[i].DataType);
            }

            // Remaining field values get assigned null
            for (int i = fieldCount; i < columns.Length; i++)
            {
                this.fields[i] = null;
            }
        }

        private object AsObject(object obj) => obj == null || (obj is string str && String.IsNullOrEmpty(str)) ? DBNull.Value : obj;

        private int CheckOrdinal(int i) => i < 0 || i >= columns.Length ? throw new ArgumentOutOfRangeException(nameof(i)) : i;

        public override object GetValue(int i) => AsObject(fields[CheckOrdinal(i)]);

        public override bool GetBoolean(int i) => Convert.ToBoolean(GetValue(i));

        public override byte GetByte(int i) => Convert.ToByte(GetValue(i));

        public override long GetBytes(int i, long dataIndex, byte[] buffer, int bufferIndex, int length)
        {
            throw new NotImplementedException();
        }

        public override char GetChar(int i) => Convert.ToChar(GetValue(i));

        public override long GetChars(int i, long dataIndex, char[] buffer, int bufferIndex, int length)
        {
            throw new NotImplementedException();
        }

        public override string GetDataTypeName(int i) => GetFieldType(i).Name;

        public override DateTime GetDateTime(int i) => Convert.ToDateTime(GetValue(i));

        public override decimal GetDecimal(int i) => Convert.ToDecimal(GetValue(i));

        public override double GetDouble(int i) => Convert.ToDouble(GetValue(i));

        public override Type GetFieldType(int i) => columns[CheckOrdinal(i)].DataType;

        public override float GetFloat(int i) => (float)Convert.ToDouble(GetValue(i));

        public override Guid GetGuid(int i) => (Guid)GetValue(i);

        public override short GetInt16(int i) => (short)GetValue(i);

        public override int GetInt32(int i) => (int)GetValue(i);

        public override long GetInt64(int i) => (long)GetValue(i);

        public override string GetName(int i) => columns[CheckOrdinal(i)].ColumnName;

        public override int GetOrdinal(string name) => Array.FindIndex(columns, n => n.ColumnName == name);

        public override string GetString(int i) => GetValue(i).ToString();

        public override int GetValues(object[] values)
        {
            if (values == null)
            {
                throw new ArgumentNullException(nameof(values));
            }

            var length = values.Length < fields.Length ? values.Length : fields.Length;
            for (int i = 0; i < length; i++)
            {
                values[i] = AsObject(fields[i]);
            }
            return length;
        }

        public override bool IsDBNull(int i) => GetValue(i) == DBNull.Value;
    }
}
